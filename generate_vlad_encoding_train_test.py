# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 10:30:39 2015

@author: ghulamahmedansari
"""

import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import MiniBatchKMeans
import numpy as np


def generate_centroids(X):
    clusterer=MiniBatchKMeans(n_clusters=100)
    clusterer.fit(X)
    return clusterer.cluster_centers_
    
   
if __name__=="__main__":
    #get the data
    X=pd.read_csv('train_X.csv',header=None)
    scaler=StandardScaler()
    scaler.fit_transform(X)

#    Y=pd.read_csv('train_Y.csv',header=None)    
#    #split into train and test
#    data_train=[]
#    class_label_train=[]
#    data_test=[]
#    class_label_test=[]
#    kf=cross_validation.StratifiedKFold(Y[0].values.tolist(),5,shuffle=True)
#    
#    for Train_index,Test_index in kf:
#        data_train.append(X.iloc[Train_index])
#        class_label_train.append(Y.iloc[Train_index])
#        data_test.append(X.iloc[Test_index])
#        class_label_test.append(Y.iloc[Test_index])
#        #WE NEED TO SCALE THE DATA
#        scaler=StandardScaler()
#        scaler.fit_transform(data_train[-1])
#        scaler.fit(data_test[-1]) 
    centroids=generate_centroids(X)
    #now do the encoding
    features=np.array(X)
    result=[]
    for feature in features[:]:
        result.append([1/(1+np.linalg.norm(feature-centroid)) for centroid in centroids[:]])
    np.savetxt('train_custom_vlad_X.csv',np.array(result),delimiter=',')
    
    

