# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 00:31:54 2015

@author: ghulamahmedansari
"""

import pandas as pd
import numpy as np

def csvtoarff(file,file_class=None, outfile=None,delimiter_=',', selectFeatures=False,features_=None, mode='train'):
    #getting class labels
    file_name=file.split('.')[0]
    data=pd.read_csv(file,sep=delimiter_,header=None)

    if selectFeatures:
        data=data[np.array(features_)-1]
        # data=data.reindex()
    num_features=data.shape[1]

    if file_class!=None:
        class_=pd.read_csv(file_class,sep=delimiter_,header=None)
    else:
        class_ = []
        for i in range(data.shape[0]):
            class_.append(1)
    data[num_features]=class_


    class_vals = map(str,xrange(100))
    # if mode=='test':
    #     class_vals.append(1)

    class_string='{'+",".join(class_vals) + '}'
    # generating headers
    header = []
    header.append('@RELATION ' + file_name + '\n\n')
    for i in range(num_features):
        header.append('@ATTRIBUTE Feat_' + str(i) + ' REAL\n')

    # if file_class!=None:
    header.append('@ATTRIBUTE class' + class_string + '\n\n')

    header.append('@DATA\n')
    # print data[:][0]
    f=open(outfile,'w')
    f.writelines(header)
    data.to_csv(f,header=None,index=None)
    f.close()

if __name__ == "__main__":
    # csvtoarff('train_X.csv','train_Y.csv','train_X.arff')
<<<<<<< HEAD
    # csvtoarff('reductionOut/X_compressed_50_L.csv','train_Y.csv','reductionOut/X_compressed_50_L.arff',delimiter_=',',selectFeatures=False, features_=features_)
    csvtoarff('reductionOut/X_test_compressed_L181_transform_old.csv',None,'reductionOut/X_test_compressed_L181_transform_old.arff',mode='test')
=======

    # features_ = [1,11,12,14,16,18,21,24,26,31,32,33,41,42,46,49,50,55,58,62,72,73,74,76,77,78,82,85,89,96,97,98,101,113,118,128,129,130,132,133,134,136,137,142,152,155,156,160,163,166,168,169,171,176,178,185,186,187,189,191,193,196,197,198,199,200,202,204,205,207,210,212,213,218,220,221,224,240,242,244,246,249,250,251,253,255,256,258,260,264,267,268,272,275,276,277,280,281,283,285,287,290,296,302,305,309,311,314,315,317,318,319,327,329,338,343,346,349,350,355,357,359,360,361,362,363,367,368,369,374,378,379,385,388,390,394,395,396,398,401,410,414,419,427,429,434,435,438,442,444,447,450,451,454,458,460,461,463,467,469,472,475,479,482,488,490,492,493,495,499,500,506,509,510,512,514,518,521,522,524,527,534,539,546,547,549,550,555,559,561,564,568,576,586,588,589,590,597,598,599,602,604,613,615,619,625,629,631,633,634,636,637,639,645,648,653,654,660,664,668,669,671,672,675,676,678,684,685,688,690,691,699,706,710,712,715,718,722,723,729,731,732,733,736,738,747,752,754,756,759,760,763,765,770,774,789,808,811,820,834,839,842,847,857,858,866,868,869,874,877,878,882,885,887,898,904,907,909,910,913,919,922,930,936,939,940,941,944,947,949,952,959,960,961,962,963,968,969,973,974,979,983,984,985,988,990,992,994,1003,1005,1008,1009,1010,1011,1014,1015,1017,1022,1023,1025,1032,1044,1047,1058,1065,1074,1093,1099,1101,1108,1110,1111,1120,1122,1130,1132,1144,1145,1151,1154,1156,1159,1164,1178,1179,1180,1202,1207,1212,1223,1236,1240,1246,1252,1260,1261,1267,1276,1278,1283,1284,1289,1295,1304,1306,1321,1323,1335,1347,1357,1364,1369,1370,1401,1403,1434,1443,1446,1468,1475,1557,1560,1569,1581,1586,1589,1629,1649,1671,1674,1677,1703,1727,1733,1747,1749,1751,1753,1766,1796,1797,1813,1825,1836,1860,1926,1928,1931,1937,1939,1946,1947,1953,1988,2001,2004,2009,2010,2016,2019,2026,2040,2041,2045,2046];
    # csvtoarff('train_X.csv','train_Y.csv','reductionOut/ForwardSelection100.arff',delimiter_=',',selectFeatures=True, features_=features_)

    features_ = [1286,1278,1357,1619,1372,1179,1926,1213,2009,1168,1236,1764,1275,1751,1797,1629,1367,2016,2045,1350,1159,1039,1836,1671,1205,1937,1931,1359,1067,1709,1074,1132,1156,1106,1122,1323,1083,1178,1108,1207,1154,1295,1086,1145,1044,1727,1296,1304,1032,1677,1276,1065,1577,1443,1047,1766,1825,1475,1860,1490,1260,1246,1434,1261,1370,1180,1240,1095,1432,1058,1590,1284,1560,1703,1110,1451,1492,1753,1120,1025,1130,2026,1366,1742,1321,1335,2019,1144,1743,1252,1401,1147,1174,1166,1606,1847,1377,1411,1420,1369]
    csvtoarff('train_custom_vlad_X.csv','train_Y.csv','reductionOut/X_custom_vlad.arff',delimiter_=',',selectFeatures=False, features_=features_)
>>>>>>> a062a4e85f96e877d46934cfaa7d65daace4de3d
