__author__ = 'csd154server'

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import cross_validation
from sklearn.metrics import f1_score
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
from sklearn.lda import LDA
import time
from scipy import stats
from sklearn import svm
from sklearn.grid_search import GridSearchCV
import cPickle


def pre2Stage(Xfile='reductionOut/X_compressed_0.8_new.csv', Yfile='train_Y.csv', Xtestfile='reductionOut/X_test_compressed_0.8_new.csv', kernel='linear'):
    X0=pd.read_csv(Xfile,header=None)
    Y0=pd.read_csv(Yfile,header=None)
    Xtest = pd.read_csv(Xtestfile, header=None)
    Ytest = do2Stage(X0, Y0, Xtest, kernel='linear')
    # np.savetxt('testOut/2stageSVM_linear.txt', Ytest, fmt='%d')

def do2Stage(X0, Y0, Xtest, kernel='linear'):
    #Stage 1:
    maxIdx = np.argmax(np.histogram(Y0.values,bins=100)[0])
    X1 = X0.values
    Y1 = (Y0.values==maxIdx).astype(int)
    # X2 = X1[:][np.where(Y1==0)]
    minorityIdx = np.where(Y1==0)
    X2 = X0.values[minorityIdx[0],:]
    Y2 = Y0.values[minorityIdx[0]]
    predOut = np.zeros(Xtest.values.shape[0])

    if kernel == 'linear':
        #linear kernel svm
        parameters = {'C':np.logspace(-3,-2,5).tolist()}
        clf_linear_cv=GridSearchCV(svm.SVC(kernel='linear'),parameters,scoring ='f1_weighted',cv=5,n_jobs=-1,verbose=True)
        clf_linear_cv.fit(X0.values,Y0.values[:,0])
        # clf_linear_cv.fit(X0.values,Y0.values[:,0])
        print 'The Best Value Of C is: ', clf_linear_cv.best_estimator_.C
        # print 'The Best Value Of gamma is: ', clf_linear_cv.best_estimator_.gamma
        print 'The Best Value Of fscore is: ', clf_linear_cv.best_score_
        fp=open('clf_pca.pkl','wb')
        # fp=open('clf.pkl','wb')
        cPickle.dump(clf_linear_cv,fp)
        fp.close()
        return 1

if __name__=="__main__":
    t0 = time.time()
    pre2Stage()
    t1 = time.time()
    print "Execution time: " + str(t1-t0) + " seconds"
