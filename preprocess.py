# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 23:32:04 2015

@author: ghulamahmedansari
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import cross_validation
from sklearn.metrics import f1_score
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
from sklearn.lda import LDA
import time
from scipy import stats
from sklearn import svm
from sklearn.grid_search import GridSearchCV
import cPickle

def doLDA(Xtrainfile, Ytrainfile, outfile, Xtestfile=None, LDAcomps=181,mode='train', normalize=True):
    Xtrain=pd.read_csv(Xtrainfile,header=None)
    Y=pd.read_csv(Ytrainfile,header=None)

    if Xtestfile!=None:
        Xtest=pd.read_csv(Xtestfile,header=None)

    #performing Linear Discriminant Analysis and also reducing dimensionality using SVD
    clf=LDA(n_components=LDAcomps)
    #projecting training data on reduced feature space
    clf.fit(Xtrain,Y[0].values)

    # if mode=='train':
    Xtr = clf.transform(Xtrain)
    # else:
    Xte = clf.transform(Xtest)

    if mode == 'train':
        X0 = Xtr
    else:
        X0 = Xte

    # Source: http://sebastianraschka.com/Articles/2014_scikit_dataprocessing.html#Linear-Transformation:-Linear-Discriminant-Analysis-%28LDA%29

    if normalize:
        scaler = StandardScaler()
        # X = pd.DataFrame(scaler.fit_transform(X))
        Xtr=scaler.fit(Xtr)
        X = pd.DataFrame(scaler.transform(X0))

    X.to_csv(outfile,header=None,index=None)


def doPCA(Xtrainfile, Xtestfile, outfiletrain, outfiletest, PCAthresh=0.8, normalize=True):
    Xtrain=pd.read_csv(Xtrainfile,header=None)
    Xtest=pd.read_csv(Xtestfile,header=None)
    reducer=PCA(n_components=PCAthresh)

    Xtrain=pd.DataFrame(reducer.fit_transform(Xtrain))

    Xtest = reducer.transform(Xtest)
    if normalize:
        scaler = StandardScaler()
        # X = pd.DataFrame(scaler.fit_transform(X))
        Xtrain=pd.DataFrame(scaler.fit_transform(Xtrain))
        Xtest = pd.DataFrame(scaler.transform(Xtest))
    Xtest.to_csv(outfiletest,header=None,index=None)
    Xtrain.to_csv(outfiletrain,header=None,index=None)

def createFolds(Xfile, Yfile, normalize=True,PCAuse=True, doFolds = False, LDAuse=False, LDAcomps=181, PCAthresh=0.8, mode='train'):
    #importing data
    flag = ""
    Y=pd.read_csv(Yfile,header=None)

    if PCAuse:
        X = doPCA(Xfile)
        #*******************SEPARATE TRAIN AND TEST SCALING!**********************

    elif LDAuse:
        X = doLDA(Xfile, Yfile, LDAcomps)
        flag = "L"

    if normalize:
        scaler = StandardScaler()
        X = pd.DataFrame(scaler.fit_transform(X))


    outfile = "reductionOut/X_compressed_" + mode + str(LDAcomps) + "_" + flag + ".csv"
    X.to_csv(outfile,header=None,index=None)

    data_train=[]
    label_train=[]
    data_test=[]
    label_test=[]
    if doFolds:
        # print '*********' , Y[0].values.tolist()
        # kf=cross_validation.KFold(Y.shape[0],5,shuffle=True)
        kf=cross_validation.StratifiedKFold(Y[0].values.tolist(),5,shuffle=True)

        for Train_index,Test_index in kf:
                data_train.append(X.iloc[Train_index])
                label_train.append(Y.iloc[Train_index])
                data_test.append(X.iloc[Test_index])
                label_test.append(Y.iloc[Test_index])

    return (data_train,label_train,data_test,label_test)

def kNN1hot(data_train,label_train,data_test,label_test,classes,k):
    label_train_1hot = encode1hot(label_train,classes)
 #    print label_train_1hot
#    print np.argmax(label_train_1hot[0])
    clf=KNeighborsClassifier(n_neighbors=k, algorithm= 'auto')
    clf.fit(data_train,label_train_1hot)
    result=clf.predict(data_test)
    return np.argmax(result,axis=1)

def doSVM(Xfile='reductionOut/X_compressed_181_L.csv', Yfile='train_Y.csv', kernel='linear'):
    X=pd.read_csv(Xfile,header=None)
    Y=pd.read_csv(Yfile,header=None)
    if kernel == 'linear':
        #linear kernel svm
        parameters = {'C':np.logspace(-4,-2,5).tolist()}
        clf_linear_cv=GridSearchCV(svm.SVC(kernel='linear'),parameters,scoring ='accuracy',cv=5)
        clf_linear_cv.fit(X.values,Y[0].values)
        print 'The Best Value Of C is: ', clf_linear_cv.best_estimator_.C
        print 'The Best Value Of accuracy is: ', clf_linear_cv.best_score_

def pre2Stage(Xfile='reductionOut/X_compressed_181_L.csv', Yfile='train_Y.csv', Xtestfile='reductionOut/X_test_compressed_L181_transform_old.csv', Ytestfile='testOut/2stageSVM_linear.txt', kernel='linear'):
    X0=pd.read_csv(Xfile,header=None)
    Y0=pd.read_csv(Yfile,header=None)
    Xtest = pd.read_csv(Xtestfile, header=None)
    Ytest = do2Stage(X0, Y0, Xtest, kernel=kernel).astype(int)
    np.savetxt(Ytestfile, Ytest, fmt='%d')

def do2Stage(X0, Y0, Xtest, kernel='linear'):
    #Stage 1:
    maxIdx = np.argmax(np.histogram(Y0.values,bins=100)[0])
    X1 = X0.values
    Y1 = (Y0.values==maxIdx).astype(int)
    # X2 = X1[:][np.where(Y1==0)]
    minorityIdx = np.where(Y1==0)
    X2 = X0.values[minorityIdx[0],:]
    Y2 = Y0.values[minorityIdx[0]]
    predOut = np.zeros(Xtest.values.shape[0])

    if kernel == 'linear':
        #linear kernel svm
        # parameters = {'C':np.logspace(-4,-2,5).tolist()}
        # parameters = {'C':[1]}
        # clf_linear_cv=GridSearchCV(svm.SVC(kernel='linear'),parameters,scoring ='accuracy',cv=5)
        # clf_linear_cv.fit(X.values,Y[0].values)
        # print 'The Best Value Of C is: ', clf_linear_cv.best_estimator_.C
        # print 'The Best Value Of accuracy is: ', clf_linear_cv.best_score_
        # clf1 = svm.SVC()


        clf1 = svm.SVC()
        clf1.set_params(C=0.056234132519, kernel='linear')
        clf1.fit(X1,Y1[:,0])

        clf2 = svm.SVC()
        clf2.set_params(C=0.00464158883361, kernel='linear')
        clf2.fit(X2,Y2[:,0])

    if kernel=='rbf':
        clf1 = cPickle.load(open('clf_2stage_1st_rbf.pkl','rb'))
        clf2 = cPickle.load(open('clf_2stage_2nd_rbf.pkl','rb'))


    Ytest1 = clf1.predict(Xtest)
    stage1out = np.where(Ytest1==1)
    stage2idx = np.where(Ytest1==0)
    Xtest2 = Xtest.values[stage2idx[0],:]
    Ytest2 = clf2.predict(Xtest2)
    predOut[stage1out[0]] = maxIdx
    predOut[stage2idx[0]] = Ytest2
    return predOut

def do1Stage(X0, Y0, Xtest, kernel='linear'):
    #Stage 1:
    if kernel == 'linear':
        # clf1 = svm.SVC()
        # clf1.set_params(C=0.0215443469003, kernel=kernel)
        # clf1.fit(X0,Y0.values[:,0])
        clf1 = cPickle.load(open('clf_pca.pkl','rb'))

    if kernel == 'rbf':
        # clf1 = svm.SVC()
        clf1 = cPickle.load(open('clf.pkl','rb'))
        # clf1.set_params(gamma=0.0056234132519, C=3.16227766017, kernel=kernel)
        # clf1.fit(X0,Y0.values[:,0])

    predOut = clf1.predict(Xtest)
    return predOut

def getScore(result,test):
    f1_result=f1_score(test,result,labels=range(100),average=None)
    return f1_result
    
def encode1hot(input,classes):
    temp=np.identity(classes)    
    output=[np.array(temp[j]) for j in input[i].values]
    return np.array(output)
    
if __name__=="__main__":
    t0 = time.time()
    # (data_train,label_train,data_test,label_test) = createFolds(doFolds=True, PCAuse=False, LDAuse=True,LDAcomps=50, PCAthresh=0.8)
    # (data_train,label_train,data_test,label_test) = createFolds(doFolds=False, PCAuse=False, LDAuse=True,LDAcomps=181, PCAthresh=0.8)
    # doSVM('linear')
    # do2Stage()
    pre2Stage(kernel='linear', Xfile='reductionOut/X_compressed_0.8_new.csv', Yfile='train_Y.csv', Xtestfile='reductionOut/X_test_compressed_0.8_new.csv', Ytestfile='testOut/1stageSVM_linear_PCA80.txt')
    # doPCA(Xtrainfile='train_X.csv', Xtestfile='test_X.csv', outfiletrain='reductionOut/X_compressed_0.8_new.csv', outfiletest='reductionOut/X_test_compressed_0.8_new.csv', PCAthresh=0.8, normalize=True)

    # doLDA(Xtrainfile='train_X.csv', Ytrainfile='train_Y.csv', outfile='reductionOut/X_test_compressed_L181_transform_old.csv', Xtestfile='test_X.csv', LDAcomps=181,mode='test')
    t1 = time.time()
    print "Execution time: " + str(t1-t0) + " seconds"


# for i in xrange(1):
#     result=kNN1hot(data_train[i],label_train[i],data_test[i],label_test[i],100,10)
#     np.savetxt('result.txt',result)
#     f1_result = getScore(result,label_test[i])
#     np.savetxt('reductionOut/f1_result_80.txt',f1_result)
#     # print stats.hmean(f1_result)
#     print np.mean(f1_result)



        # clf1.set_params(nu=0.5, cache_size=200, coef0=0.0,
        #                 degree=3, gamma='auto', kernel='linear',
        #                 max_iter=-1, random_state=None, shrinking=True,
        #                 tol=0.001, verbose=False)

