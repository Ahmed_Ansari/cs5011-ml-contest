__author__ = 'csd154server'

import numpy as np
from scipy import stats
import os
import re
import arff

def getFscoreHM_onlytable(tablefile):
    f = open(tablefile,'r')
    #for line in f.readlines():
    lines = f.readlines()
    lines = map(lambda x: x.split(),lines)

    #linearray = np.vstack(lines)
    linearray = np.array(lines)

    linearray_float = np.zeros(linearray.shape)
    for i in xrange(linearray.shape[0]):
        for j in xrange(linearray.shape[1]):
            linearray_float[i,j] = float(linearray[i,j])

    # print np.min(linearray_float[:,4])
    # print stats.hmean(linearray[:,4])n
    print len(linearray_float[:,4]) / np.sum(1.0/linearray_float[:,4])

def getFscoreHM_resultbuffer(resultbuffer):
    f = open(resultbuffer,'r')
    #for line in f.readlines():
    beginread = False
    lines = []
    for line in f.readlines():
        if not beginread:
            if len(line.split())==0:
                continue
            if line.split()[0]!='TP':
                continue
            else:
                beginread=True
        else:
            if line.split()[0]=='Weighted':
                break
            else:
                lines.append(line.split())
    if len(lines)>0:
        linearray = np.array(lines)
        linearray_float = np.zeros(linearray.shape)
        for i in xrange(linearray.shape[0]):
            for j in xrange(linearray.shape[1]):
                linearray_float[i,j] = float(linearray[i,j])
        # score = len(linearray_float[:,4]) / np.sum(1.0/linearray_float[:,4])
        score = np.mean(linearray_float[:,4])
        if score < 0.85:
            score = -1
        # print np.min(linearray_float[:,4])
    else:
        score = -1
    return score

def getPerfFromDir(dirname):
    files = os.listdir(dirname)
    f = open(dirname+'scores','w')
    sortedfiles = np.sort(files)
    for file in sortedfiles:
        if file.endswith('txt'):
            resultbuffer = dirname + file
            score = getFscoreHM_resultbuffer(resultbuffer)
            if score != -1:
                f.write(file + ': Score = ' + str(score) + '\n')
    f.close()

def getTestLabelsFromDir(dirname, colnum):
    files = os.listdir(dirname)
    for file in files:
        if not file.endswith('.arff'):
            continue
        output = []
        filepath = dirname + file
        for row in arff.load(filepath):
            output.append(row.predictedclass)
        fout = open(dirname+file.split('.')[0]+'.txt','w')

        for item in output:
            print>>fout, item

if __name__ == "__main__":
    # getPerfFromDir('wekaOut/')
    getTestLabelsFromDir('testOut/',-2)